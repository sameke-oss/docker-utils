#! /bin/bash

# This script builds a docker image with the appropriate tags.
# Environment Variables used:

# PREFIX: A prefix applied to the version number when creating the tag (ex: 'arm')
# 
# The following environment variables should come from version.sh
# VERSION: The version number added to the Docker image tag
# NAME: The name of the docker image
# REGISTRY: The url to the registry
# REL_PATH: the relative path of the git repo
# 

LOCAL=0

echo "Checking for version.sh file"
if [ -f "./version.sh" ]
then
	source ./version.sh

	while getopts lr: option; do
		case $option in
            r)
            REGISTRY=${OPTARG};;
			l)
			LOCAL=1;;
			*)
			echo -e "USAGE:\n";;
		esac
	done

	echo Registry = ${REGISTRY}
	# Get the relative path
	REL_PATH=$(git remote get-url origin | sed -e 's~.*\.com/~~; s~\.git$~~')
	TAG="$PREFIX"$VERSION

	echo "Tag = $TAG"
	
    if [ "${LOCAL}" == "1" ]; then
		echo "** LOCAL BUILD: build --builder=container -t $REGISTRY$REL_PATH:$TAG . **"
	 	docker build --no-cache -t $REGISTRY$REL_PATH:$TAG --build-arg GITLAB_ACCESS_TOKEN=${SAMEKE_ACCESS_TOKEN} --build-arg GITLAB_USER=${SAMEKE_USER} .
	else
	 	echo "**docker buildx build --builder=container --push --platform=linux/amd64,linux/arm64 -t $REGISTRY$REL_PATH:$TAG ."
	 	docker buildx create --name container --driver=docker-container
	 	# docker buildx build --builder=container --push --platform=linux/amd64,linux/arm64 --provenance false -t $REGISTRY$REL_PATH:$TAG .
		docker buildx build --builder=container --push --platform=linux/amd64 --provenance false -t $REGISTRY$REL_PATH:$TAG .
	fi
else
	echo "Could not find version.sh file. The version.sh file should set repo-specific env variables (e.g. version number). Please execute the script in a repo that contains a version.sh file"
fi