FROM debian:bookworm-slim
WORKDIR /build
RUN apt update -y
RUN apt install build-essential git pkg-config libtool autoconf wget ca-certificates software-properties-common libssl-dev libgmp-dev python3-pip curl -yq
RUN apt install bash-static -yq
COPY ./scripts/* /build
# RUN ./install-cmake.sh
RUN ./install-node.sh
# RUN ./install-protobuf.sh
WORKDIR /
RUN rm -rf /build