#!/bin/bash

apt purge cmake

CMAKE_TAG=3.29.6

mkdir -p ./tmp
cd ./tmp
wget https://github.com/Kitware/CMake/releases/download/v${CMAKE_TAG}/cmake-${CMAKE_TAG}.tar.gz
tar -xzf ./cmake-${CMAKE_TAG}.tar.gz
cd ./cmake-${CMAKE_TAG}
./bootstrap
make -j8
make install
cd ../..
rm -rf ./tmp