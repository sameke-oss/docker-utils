#!/bin/bash

# remove old version of node we may have installed
NODE_VERSION=20.15.0
rm -rf /opt/node-*
mkdir -p ./tmp
curl -o ./tmp/node.tar.xz https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.xz
tar -xf ./tmp/node.tar.xz -C ./tmp
nodedir=$(find ./tmp -type d -name node* -prune)
outputdir=/opt/node-v${NODE_VERSION}
mv $nodedir $outputdir
set -o noclobber
# echo "export PATH=$PATH:$outputdir/bin" >> /etc/environment
ln -s $outputdir/bin/node /usr/local/bin/node
ln -s $outputdir/bin/npm /usr/local/bin/npm
npm install -g tsc
ln -s $outputdir/bin/tsc /usr/local/bin/tsc
rm -rf ./tmp
echo $(node -v)