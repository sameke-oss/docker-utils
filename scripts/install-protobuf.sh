#!/bin/bash

# NOTE: This script is dependent upon the following packages:
# autoconf automake libtool curl make g++ unzip

# TODO: add warning about this deleting old versions found in /usr/local/lib
PROTO_VERSION=v5.27.2

echo "**** Searching for protobuf dependency..."
PROTO_PATH=$(which protoc)

if test ! -z $PROTO_PATH
then
    echo "Will replace found protobuf version $(protoc --version) with ${PROTO_VERSION}"
fi

rm /usr/local/lib/libproto*
rm /usr/local/lib/libabsl*

mkdir -p ./tmp
cd ./tmp
git clone -b ${PROTO_VERSION} --single-branch  https://github.com/protocolbuffers/protobuf.git
cd protobuf
git submodule update --init --recursive

# cmake -DCMAKE_BUILD_TYPE=Release -Dprotobuf_BUILD_TESTS=OFF -Dprotobuf_BUILD_SHARED_LIBS=ON .
cmake -DCMAKE_BUILD_TYPE=Release -Dprotobuf_BUILD_TESTS=OFF .
cmake --build . --parallel $(nproc --all)
make install

# Clean up
cd ../..
rm -rf ./tmp